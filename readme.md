
# ARTCAK Laravel Starter

Project starter yang dibangun dengan [Laravel 5.4](https://laravel.com/docs/5.4) untuk memudahkan inisiasi proyek aplikasi web dengan fitur-fitur umum seperti: manajemen pengguna, autentikasi halaman dasbor, autentikasi API, dan lainnya :sunglasses:.

> **Open for contribution!** Silakan tambahkan fitur, kode baru, pembaharuan dokumentasi atau apapun yang bisa meningkatkan manfaat project ini. Termasuk support untuk Laravel 5.5+ yang belum tersedia pada versi ini :v:.

### Daftar isi

- [Apa saja yang tersedia di sini](#apa-saja-yang-tersedia-di-sini)
- [Quick Start](#quick-start)
- [Fitur Dasar](#fitur-dasar)
    - [Autentikasi via tampilan web](#autentikasi-via-web)
    - [Manajemen user, role, dan permission](#manajemen-user-role-dan-permission)
    - [Seeder untuk sampel user, role, dan permission](#seeder-untuk-sampel-user-role-dan-permission)
    - [Format respon API](#format-respon-api)
        - [API Testing (Opsional)](#api-testing-opsional)
    - [Class untuk Firebase Push Notification sederhana](#firebase-push-notification)
    - [Class untuk SMS Notification sederhana](#sms-notification)
- [Ingin menggunakan docker dan docker compose? (Opsional)](#ingin-menggunakan-docker-dan-docker-compose-opsional)


### Apa saja yang tersedia di sini?

- [laravelcollective/html](https://laravelcollective.com/docs/5.3/html) yang membantu mempermudah pembuatan elemen HTML terutama form pada template blade Laravel.
- [doctrine/dbal](https://packagist.org/packages/doctrine/dbal) untuk mendukung beberapa kemampuan migrasi (migration) pada Laravel.
- [ramsey/uuid](https://packagist.org/packages/ramsey/uuid) untuk membuat id data dalam bentuk UUID (bukan integer autoincrement) untuk kasus-kasus data tertentu.
- [santigarcor/laratrust](https://github.com/santigarcor/laratrust) untuk manajemen hak akses (permission) berdasarkan role user yang berbeda-beda.
- [Adminator Admin Dashboard](https://github.com/puikinsh/Adminator-admin-dashboard)
- [Swagger UI](https://swagger.io/swagger-ui/) untuk dokumentasi API sebagai alternatif Postman jika dibutuhkan.
- [Bower](https://bower.io/) untuk mempermudah proses mengunduh library=library yang digunakan untuk tampilan, misalnya JQuery, dan library-library Javascript lainnya.
- [docker compose](https://docs.docker.com/compose) konfigurai kontainer (berbasis docker) yang bisa dimanfaatkan untuk menjalankan development server dengan dukungan nginx dan php-fpm.

### Quick Start

- Download atau clone repositori: `git clone https://gitlab.com/artcak/starterProject/web/laravel-starter.git`
- Masuk ke direktori laravel-starter
- Salin isi dari `.env.example` ke file baru dan simpan dengan nama `.env`
- Pada file `.env` tadi atur konfigurasi database pada parameter berikut:
    ```
    DB_CONNECTION=mysql
    DB_HOST=127.0.0.1
    DB_PORT=3306
    DB_DATABASE=contohdb
    DB_USERNAME=contohuser
    DB_PASSWORD=contohpassword
    ```
- Jalankan `composer install` untuk menginstall semua dependensi
- Jalankan `composer initiate`
    Perintah ini akan menjalankan 2 perintah yang disediakan Laravel, yaitu:
    1. `php artisan key:generate` untuk membuat kumpulan karakter acak yang menjadi value dari parameter `APP_KEY` pada file `.env` untuk mendukung fungsi-fungsi enkripsi dan keamanan di Laravel.
    2. `php artisan project:fresh`. Sebuah perintah tambahan yang disediakan project starter untuk menjalankan `php artisan migrate:reset`, `php artisan migrate`, lalu `php artisan db:seed` secara berturut-turut, sehingga project siap dijalankan dengan data-data sampel.
- Selamat menggunakan project starter seperti menggunakan Laravel pada umumnya :smile:

#### Fitur Dasar

Fitur-fitur dasar aplikasi web yang sudah diimplementasikan dan siap pakai pada project starter ini sebagai berikut:

- Autentikasi (login, logout, reset password, register) via tampilan web dengan cara yang sudah disediakan Laravel ([lihat](#autentikasi-via-web))
- Manajemen user, role, dan permission ([lihat](#manajemen-user-role-dan-permission))
- Seeder untuk sampel user, role, dan permission ([lihat](#seeder-untuk-sampel-user-role-dan-permission))
- Format respon API ([lihat](#format-respon-api))
    - API Testing (Opsional) ([lihat](#api-testing-opsional))
- Class untuk Firebase Push Notification sederhana ([lihat](#firebase-push-notification))
- Class untuk SMS Notification sederhana ([lihat](#sms-notification))


### Ingin menggunakan docker dan docker compose? (Opsional)

#### Mengapa docker?

Docker memungkinkan kita menjalankan `container` untuk mengisolasi project pada lingkungan yang sudah terinstall service-service yang dibutuhkan seperti web server dan PHP. Bayangkan container ini seperti virtual machine, hanya saja kemampuannya tidak selengkap virtual machine. Pelajari selengkapnya di sini: https://www.docker.com/what-docker. 

Singkatnya, jika kita punya beberapa project dan masing-masing perlu lingkungan berbeda, misalnya project A butuh PHP 5 dan lainnya PHP 7, atau requirement-requirement unik lainnya, dengan docker kita tidak perlu menginstall dependensi-dependensi itu di komputer yang digunakan untuk development. Cukup dengan docker, kita bisa menjalankan project yang berbeda-beda itu karena dependensi-dependensi tadi terinstall di dalam container,bukan di komputer kita langsung. Ketika ingin mengerjakan project saja kita jalankan containernya, jika tidak digunakan biarkan dia tertidur atau nonaktif. Cukup menghemat penggunaan memory dan CPU :tada:.

#### Lalu apa itu docker-compose?

Misal jika project kita butuh 1 container untuk menjalankan nginx dan 1 container lagi untuk php-fpm maka setiap ingin menjalankan project kita perlu menyalakan 2 container. `docker-compose` mempermudah langkah kita sehingga bisa menyalakan beberapa container secara bersamaan hanya dengan 1 baris perintah :sunglasses:.

Mengapa pada contoh di atas nginx dan php-fpm dipisah menjadi container yang berbeda? Apa tidak bisa dijadikan satu? Bisa saja, tergantung selera. Kebetulan pada project ini kita mencoba menyimulasikan bagaimana jika pada saat production terdapat 2 server, 1 untuk nginx lalu php-fpm nya di server lain. Strategi ini (memisahkan nginx dengan php-fpm) salah satu yang biasa dipakai untuk kebutuhan scaling aplikasi agar 1 server khusus berperan untuk menerima request (nginx) dan di belakangnya ada beberapa server aplikasi yang memroses request dengan php-fpm.

#### Quick Start docker-compose

> Langkah-langkah berikut dicoba pada sistem operasi berbasis Linux dengan distribusi Ubuntu. Kemungkinan juga akan berhasil pada distribusi Linux lainnya atau Mac OS. Untuk penggunaan pada Windows OS harap membaca dokumentasi official dari [Docker](https://docs.docker.com/) dan [docker-compose](https://docs.docker.com/compose/). 
> Jika sudah menggunakan OS berbasis Linux namun tetap tidak bisa menjalankan Docker maupun docker-compose, harap membaca kembali dokumentasi official mereka karena mungkin saja untuk versi OS yang digunakan tidak mendapat support dari Docker. Ada beberapa kasus seperti ini terutama untuk OS berbasis Linux versi lama :v:.

- Pastikan Anda sudah menginstall [Docker](https://docs.docker.com/engine/installation). Silakan kunjungi [dokumentasi official Docker](https://docs.docker.com/engine/installation/) untuk informasi lebih lanjut.
- Pastikan Anda sudah menginstall [Docker Compose](https://docs.docker.com/compose). Silakan kunjungi [dokumentasi official docker-compose](https://docs.docker.com/compose/install) untuk informasi lebih lanjut.
- Ubah semua bagian pada file `compose/webserver/webserver.dockerfile` yang bertuliskan `<your-user-name>` dengan nama user Anda yang sedang aktif pada komputer development. Ini bertujuan untuk membuat user yang nantinya diberi hak menjalankan web server dan mengakses file-file pada direktori web server. Konfigurasi default user untuk web server adalah `www-data` yang akan menimbulkan masalah permission ketika kita mengedit file project dengan user selain `www-data`.
- Ubah semua bagian pada file `compose/phpfpm/phpfpm.dockerfile` yang bertuliskan `<your-user-name>` dengan nama user Anda yang sedang aktif pada komputer development. Kali ini untuk melakukan hal yang sama pada `php-fpm`.
- Ubah semua bagian pada file `compose/phpfpm/www.conf` yang bertuliskan `<your-user-name>` dengan nama user Anda yang sedang aktif pada komputer development. Ini untuk mendaftarkan user yang sudah dibuat pada 2 langkah sebelumnya pada konfigurasi `php-fpm` sehingga bisa mengeksekusi file-file project ketika diakses melalui web server.
- Pastikan Anda berada pada direktori project, atau dengan kata lain pada direktori dimana file `docker-compose.yml` berada. Karena perintah `docker-compose` akan mencari file tersebut sebagai acuan dalam menjalankan container.
- Jalankan `docker-compose up` untuk menjalankan semua container yang sudah diatur pada konfigurasi docker-compose, atau `docker-compose -d` untuk melakukan hal yang sama tapi membuatnya berjalan di background dan tidak memblokir layar terminal yang sedang aktif. Jalankan `docker-compose help` jika ingin melihat daftar perintah beserta opsi yang tersedia.
- Akses project melalui URL `localhost:21000`.
- Jika ingin mengganti port bisa dengan cara mengubah file `docker-compose.yml` dan ganti angka 21000 dengan port yang diinginkan:
    ```
    ports:
      - "21000:80"
    ```
- Selamat menggunakan project starter seperti menggunakan Laravel pada umumnya :smile:

### Autentikasi via Web

Fitur dasar autentikasi via web sudah diimplementasikan sesuai [dokumentasi](https://laravel.com/docs/5.4/authentication) meliputi: login, logout, register, dan reset password.

> Jika seeder sudah dijalankan, maka Anda bisa mencoba autentikasi menggunakan data-data user yang tersedia pada file `database/seeds/BasicAuthenticationsSeeder.php`.

### Manajemen User, Role, dan Permission

Library [santigarcor/laratrust](https://github.com/santigarcor/laratrust) sudah terinstall dan siap digunakan. Silakan baca dokumentasi tersebut untuk menerapkan cara menyeleksi fitur berdasarkan role atau permission. Sedikit modifikasi yang ditambakan adalah kolom `built-in` pada tabel user, role dan permission yang bisa digunakan sebagai penanda bahwa user, role dan permission default aplikasi tidak bisa dihapus (Lihat file migration `database/migrations/2018_03_19_223317_laratrust_setup_tables.php`).

> Catatan: pembatasan agar data-data built-in tidak bisa dihapus tetap perlu kode khusus di setiap fungsi hapus controller. Bukan serta merta hanya karena menambahkan kolom built-in maka data pasti tidak bisa dihapus. Contoh penggunaannya bisa dilihat di controller `app/Http/Controllers/Dashboard/UserAdministrations/UsersController.php` pada fungsi `destroy`.

> Jika seeder sudah dijalankan, maka Anda bisa mencoba autentikasi menggunakan data-data user yang tersedia pada file `database/seeds/BasicAuthenticationsSeeder.php`.

### Seeder untuk Sampel User, Role, dan Permission

Seeder untuk sampel user, role, dan permission terdapat pada file `database/seeds/BasicAuthenticationsSeeder.php`. Silakan edit file ini atau tambahkan file lain sesuai kebutuhan.

### Format Respon API

Respon API yang konsisten akan mempermudah kordinasi dengan mobile developer karena format data yang diberikan memiliki pola yang sama. Untuk mencapai hal ini maka disedikan fungsi-fungsi pembantu yang akan men-generate format api berdasarkan data yang ingin dikembalikan kepada client.

Tersedia 2 jenis format API yang bisa digunakan.

1. API telanjang, yakni respon API yang dikembalikan apa adanya tanpa ada tambahan parameter pembungkus.
    Misalnya ketika mengakses api daftar user maka respon API langsung memberikan array hasil query:
    ```
    [
        {
            "username": "contoh1",
            "email: "contoh1@mail.com"
        },
        {
            "username": "contoh2",
            "email: "contoh2@mail.com"
        }
    ]
    ```
    Contoh penggunaannya bisa dilihat pada `app/Http/Controllers/Api/V1/BasicApiController.php`. Format API ini didukung oleh fungsi-fungsi yang tersedia pada `app/Http/ApiResponder.php`.
2. API terbungkus, yakni respon API yang dikembalikan dengan tambahan parameter-parameter lain yang membungkus data aslinya. Format ini yang umum digunakan ketika ingin berkomunikasi dengan aplikasi mobile untuk mempermudah logika pada aplikasi mobile menentukan langkah yang harus dijalankan berdasarkan informasi tambahan yang kita sediakan berdampingan dengan data yang diminta.
    Misalnya ketika mengakses api daftar user maka respon API akan menjadi:
    ```
    {
        "response_code": 200,
        "message": "Daftar user berhasil didapatkan",
        "errors": null,
        "data": [
            {
                "username": "contoh1",
                "email: "contoh1@mail.com"
            },
            {
                "username": "contoh2",
                "email: "contoh2@mail.com"
            }
        ]
    }
    ```
    Contoh penggunaannya bisa dilihat pada `app/Http/Controllers/Api/V1/EncapsulatedApiController.php`. Format API ini didukung oleh fungsi-fungsi yang tersedia pada `app/Http/EncapsulatedApiResponder.php`.

    Aturan yang berlaku untuk format ini antara lain:
    - Jika request berhasil diproses maka **response_code bernilai 2xx**, **message diisi pesan berhasil**, **errors diset null**, dan **data diisi oleh data yang diminta**
    - Jika request berhasil diproses tapi tidak memerlukan kembalian data maka **data bisa diset null**, sisanya mengikuti poin sebelumnya
    - Jika request gagal maka **response_code bernilai selain 2xx**, **message diset null**, **errors berupa array berisi pesan error**, dan **data diset null**

Fungsi yang tersedia pada `app/Http/ApiResponder.php` dan `app/Http/EncapsulatedApiResponder.php` merangkup format API ke dalam jenis-jenis berikut:

- **success** (kode HTTP 2xx): respon ketika request berhasi diproses tanpa ada masalah.
- **failure** (kode HTTP 400): respon ketika request gagal diproses karena kesalahan client, misalnya memasukan kode OTP yang salah, melakukan aktivasi pada akun yang sudah aktif, dll.
- **unauthorized** (kode HTTP 401): respon ketika request tidak bisa dipenuhi karena user tidak memiliki hak akses pada fitur tersebut, biasanya ketika token login tidak valid, tidak disertakan atau kadaluarsa.
- **forbidden** (kode HTTP 403): respon ketika request tidak bisa dipenuhi karena user tidak memiliki hak akses pada fitur tersebut namun token login valid, biasanya ketika fitur tertentu hanya boleh diakses role tertentu, misalnya lihat daftar user hanya boleh oleh role admin saja, sehingga role pegawai tidak boleh mengakses fitur itu.
- **notFound** (kode HTTP 404): respon mengembalikan pesan gagal karena data yang dicari tidak ada, misalnya ketika ingin mengakses `/users/100` padahal user dengan id 100 tidak ada.
- **invalidParameters** (kode HTTP 422): respone gagal ketika request tidak menyertakan payload yang sesuai, misalnya ketika ingin tambah user tapi tidak menyertakan username pada POST data.
- **customResponse**: respon kustom sesuai kebutuhan. Dengan fungsi ini kita bisa membuat respon API dengan kode, message, data dan errors sesuai keinginan namun tetap mengikuti format yang konsisten.

#### API Testing (Opsional)

##### Mengapa perlu API testing?

Testing API biasanya hanya melalui Postman dengan mengirim request dengan payload yang sudah didefinisikan. Bagaimana jika ternyata ada kasus-kasus tertentu yang mungkin terjadi, misal ketika parameter payload tidak lengkap, data yang dicari tidak ditemukan, atau token tidak valid? Tentu butuh waktu lama jika ingin mencoba semua kemungkinan request melalui Postman, mencoba 1 request yang sukses saja butuh waktu karena harus mengubah-ubah payload lalu klik kirim dan memeriksa satu per satu parameter responnya. Belum lagi jika kita menambahkan kodingan baru, maka kita perlu melakukan testing API lagi untuk memastikan tidak ada yang error karena perubahan yang baru. Di sinilah API testing berperan untuk mempercepat proses testing hanya dengan menjalankan 1 baris perintah pada terminal. Kita bisa melakukan testing semua API berulang-ulang dalam hitungan detik :sunglasses:, tentunya waktu akan semakin lama sesuai dengan jumlah API yang dites, bisa jadi sampai sekian menit, tapi setidaknya lebih cepat daripada klik-klik manual di Postman :speak_no_evil:.

##### Sekilas konsep API testing

- Kita membuat kode test untuk mengetes kode program sesungguhnya
- Kode test ini jika dijalankan akan menyimulasikan akses ke route yang telah kita didefinisikan
- kode test akan membaca respon dan memeriksa apakah parameter yang dikembalikan oleh route sudah sesuai keinginan

##### Bagaimana cara melakukan API testing

Laravel telah mempermudah proses API testing ini karena telah menginstall library testing [PHPUnit](https://phpunit.de/) dan membuatkan fungsi-fungsi bantuan untuk menggunakan library itu dengan mudah pada project berbasis Laravel. Dokumentasi lengkap mengenai API testing bisa dibaca pada https://laravel.com/docs/5.4/http-tests.

Semua kode test akan ditempakan pada direktori `tests`. Untuk project starter ini sudah tersedia testing untuk sampel APi pada direktori `tests/Feature/Api/V1`. Fungsi-fungsi pembantu juga sudah tersedia pada `tests/Feature/Api/V1/BasicApiTest.php` dan `tests/Feature/Api/V1/EncapsulatedApiTest.php` untuk mempersingkat pemeriksaan-pemeriksaan respon yang umum dilakukan.

Untuk menjalankan test cukup dengan menjalankan perintah `phpunit` pada direktori project maka semua kode test akan dieksekusi satu per satu. PHPUnit menyediakan beberapa opsi untuk menjalankan testing. Jika dilihat pada file `composer.json` untuk menjalankan testing bisa menggunakan perintah `composer test`, `composer test-testdox`, atau `composer test-standalone`. Masing-masing perintah memiliki cara yang berbeda dalam menjalankan test. Khusus untuk `composer test-standalone` digunakan untuk menjalankan testing pada Gitlab CI. What? Apa lagi itu Gitlab CI? ([baca selengkapnya](#gitlab-continuous-integration-service)).


##### Gitlab Continuous Integration Service

Membuat API testing saja belum cukup untuk membuat *developer bahagia* :blush:. Ada kalanya kita lupa menjalankan testing di komputer development, lalu tanpa sadar sudah melakukan push ke git. Tanpa disadari pula kode yang kita tambahkan ternyata merusak fungsi-fungsi yang sebelumnya baik-baik saja (seharusnya ini bisa dideteksi sejak dini jika kita ingat menjalankan test). Di sinilah Gitlab Continuous Integration Service atau kita sebut saja Gitlab CI berperan. Gitlab CI ini mampu menjalankan sebuah server yang menyimulasikan kondisi ketika project dideploy maupun dites. Jadi, dengan bantuan Gitlab CI ini kita bisa menjalankan API testing tadi setiap ada aktivitas push ke git. Gitlab akan mendeploy project kita pada container (container? hmmm :blush:) dan menjalankan step-step yang kita inginkan. Step-step yang dimaksud bisa dilihat pada file `.gitlab-ci.yml`. File ini adalah file wajib yang harus tersedia pada root direktori project kita jika ingin menggunakan layanan Gitlab CI. Bisa dilihat pada file itu terdapat baris berikut:
```
test:
  script:
    - php composer.phar test
```

Yap, kita meminta Gitlab CI untuk menjalankan perintah `php composer.phar test` atau sama artinya dengan `php composer test` atau `composer test`. Ini artinya kita menjalankan API testing yang telah dibuat tadi di server Gitlab. Bagaimana kita tahu jika hasil testingnya berhasil atau gagal?

![ci-badge](doc/ci-badge.PNG "CI badge")

Jika menggunakan Gitlab CI maka di setiap commit yang dilakukan pada project akan diberi lencana sukses (centang hijau) atau gagal (silang merah). Jika kita menelusuri tautan pada lencana itu maka akan diarahkan pada tampilan hasil menjalankan Gitlab CI. Pada tampilan ini akan muncul output terminal dari setiap step yang didefinisikan pada file `.gitlab-ci.yml`. Jika ada salah satu step saja yang gagal maka proses CI akan gagal dan commit dianggap tidak aman untuk dideploy ke production. Dengan ini kita bisa mendeteksi masalah sebelum masuk server production, sehingga mengurangi shock jika respon API gagal ketika dicoba pada perangkat mobile :sunglasses:.

##### API Testing + CI, Penting atau Tidak?

Beda developer mungkin akan beda pandangan mengenai perlu tidaknya melakukan testing dan CI ini. Beberapa keluhan yang mungkin muncul:
- memperlambat pengerjaan project
- developer perlu menulis kode lebih banyak

Ya memang benar :joy:. Jika baru pertama kali menerapkan testing (biasanya orang menyebutnya TDD: Test Driven Development) akan terasa berat dan lama, karena kita ibaratnya menulis kode "dua kali lebih banyak" dari seharusnya. Tapi percayalah, di Indonesia sudah mulai banyak yang mengadopsi budaya TDD ini karena manfaatnya yang begitu terasa untuk **jangka panjang** dan ketika project sudah berjalan cukup lama. Developer akan lebih percaya diri akan kode yang ditulisnya serta aplikasi bisa dijaga kualitasnya karena testing keseluruhan API bisa dilakukan sejak proses development dalam waktu singkat untuk semua kasus termasuk worst case sekalipun. Jika Anda tertarik silakan cari sumber informasi mengenai TDD. Jika berminat bisa diterapkan, jika tidak bisa diabaikan. Itulah mengapa API testing ini opsional untuk project starter ini. Tapi jika ingin diterapkan, fungsi-fungsi dasarnya sudah tersedia dan siap pakai :sunglasses:.


#### Firebase Push Notification

Untuk mengirim Firebase Push Notification, melibatkan langkah berikut:

1. Atur API Key Firebase pada parameter `FIREBASE_SERVER_KEY` file `.env`. API key ini bisa didapatkan pada konfigurasi project Firebase di halaman web Firebase Console.
2. Menyiapkan payload request sesuai format yang ditentukan Firebase
3. Mengakses URL Firebase dan menyertakan payload request pada poin 1
4. Firebase akan memeriksa apakah payload valid
5. Jika payload valid makan notifikasi akan diteruskan ke device yang sesuai
6. Firebase memberikan respon sesuai status pengiriman notifikasi
7. Simpan respon dari Firebase untuk proses debug jika dibutuhkan (opsional)

Untuk mempermudah proses ini kita gunakan kemampuan Notification dari Laravel. Silakan baca selengkapnya di sini: https://laravel.com/docs/5.4/notifications.

Karena Laravel tidak menyediakan channel notifikasi untuk Firebase maka ini termasuk Custom Channel Notification. Untuk notifikasi kustom ini kita perlu mendefinisikan channel notifikasi untuk Firebase, sehingga file-file yang terlibat untuk proses ini adalah:

- `app/ThirdParties/Firebase/FirebaseConnection.php` untuk membantu proses terkoneksi dengan URL Firebase
- `app/ThirdParties/Firebase/FirebasePushNotification.php` untuk membantu membentuk payload request sesuai format Firebase
- `app/Notifications/Channels/Firebase.php` channel notifikasi karena belum disediakan Laravel. Baca: [Laravel Custom Channel Notification](https://laravel.com/docs/5.4/notifications#custom-channels)
- `app/Notifications/FirebaseNotif.php` class notifikasi untuk mengirim Firebase Push Notification.

Contoh penggunaan Firebase Push Notification bisa dilihat pada test `tests/Feature/Notifications/FirebaseNotificationTest.php`. Singkatnya kita hanya perlu menambahkan potongan kode berikut:
```
$title = 'Notification Title';
$body = 'This is notification body.';
$label = 'notification_test';
$user->notify(new FirebaseNotif($title, $body, $label));
```
Dengan `$user` adalah object user yang ingin dikirimkan notifikasi. 

> Perlu diingat jika ingin mengirimkan Firebase Push Notification maka pastikan kolom `firebase_token` pada user sudah terisi dengan token Firebase user. Jika belum terisi maka notifikasi tidak akan terkirim ke manapun. Kolom ini sudah ditambahkan ketika melakukan migrasi tabel user dengan file `database/migrations/2014_10_12_000000_create_users_table.php`.

#### SMS Notification

SMS Notification biasanya dipakai untuk mengirim kode OTP atau pengingat. Untuk mengirim notifkasi SMS ini langkah yang terlibat sebagai berikut:

1. Tentukan layanan SMS Gateway yang ingin digunakan
2. Biasanya layanan tersebut akan memberikan credential (berupa user dan password) yang bisa digunakan untuk mengirim sms menggunakan layanan mereka
3. Set value parameter `ZENZIVA_URL`, `ZENZIVA_USERKEY`, dan `ZENZIVA_PASSKEY` sesuai credential yang didapatkan. Jika menggunakan layanan lain silakan buat parameter baru pada file `.env` sesuai kebutuhan.
4. Siapkan payload request sesuai format yang diminta oleh layanan SMS Gateway
5. Akses URL yang disediakan layanan dengan menyertakan payload request
6. Layanan akan memberikan respon hasil dari proses pengiriman 
7. Simpan respon untuk proses debug jika dibutuhkan (opsional)

SMS Notification sebenarnya sudah disediakan oleh Laravel, hanya saja channel yang digunakan adalah Nexmo, sedangkan rata-rata proyek yang kita jalankan menggunakan layanan SMS Gateway lokal. Untuk itu perlu dibuat channel baru, atau dengan kata lain kita perlakukan SMS Notification ini sebagai Custom Channel Notification. File-file yang terlibat untuk proses ini, antara lain:

- `app/ThirdParties/Sms/SmsNotification.php` untuk membentuk object notifikasi yang bisa dipakai di class lainnya.
- `app/ThirdParties/Sms/ZenzivaConnection.php` sebagai contoh class untuk mengurus koneksi dengan layanan SMS Gateway Zenziva. Jika ingin menggunakan layanan selain Zenziva, bisa membuat class baru dan meniru struktur dari class ini sehingga cara penggunaannya pun bisa disamakan.
- `app/Notifications/Channels/Sms.php` channel notifikasi karena belum disediakan Laravel. Baca: [Laravel Custom Channel Notification](https://laravel.com/docs/5.4/notifications#custom-channels)
- `app/Notifications/SmsNotif.php` class notifikasi untuk mengirim SMS Notification.

Contoh penggunaan SMS Notification bisa dilihat pada test `tests/Feature/Notifications/SmsNotificationTest.php`. Singkatnya kita hanya perlu menambahkan potongan kode berikut:
```
$message = 'This is a message';
$user->notify(new SmsNotif($message));
```
Dengan `$user` adalah object user yang ingin dikirimkan notifikasi. 

> Perlu diingat jika ingin mengirimkan SMS Notification maka pastikan kolom `phone_number` pada user sudah terisi dengan token nomor telepon user yang bisa dihubungi. Jika belum terisi maka notifikasi tidak akan terkirim ke manapun. Tapi jika sudah terisi namun tidak bisa dihubungi maka respon dari layanan akan memberitahu bahwa pengiriman gagal. Kolom ini sudah ditambahkan ketika melakukan migrasi tabel user dengan file `database/migrations/2014_10_12_000000_create_users_table.php`.

## Lisensi
[MIT license](http://opensource.org/licenses/MIT).
