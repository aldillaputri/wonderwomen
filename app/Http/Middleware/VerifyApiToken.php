<?php

namespace App\Http\Middleware;

use Closure;
use App\Http\EncapsulatedApiResponder;
use App\User;

class VerifyApiToken
{
    use EncapsulatedApiResponder;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->has('token')) {
            $user = User::where('token_login_api', $request->token)->first();
            if ($user) {
                $request->setUserResolver(function() use ($user) {
                    return User::find($user->id);
                });
                return $next($request);
            }
        }
        return $this->unauthorized();
    }
}
