<?php

namespace App\Http\Controllers\Endpoint;

use Illuminate\Http\Request;
use App\ModelEndPoint;
use DB;
use Ramsey\Uuid\Uuid;
use App\Http\Controllers\Controller;
use App\ModelProject;

class EndPoint extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $endpoints = ModelEndPoint::all();

       return view('endpoint.index',compact('endpoints'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $projects = ModelProject::orderBy('name', 'ASC')->get();

        return view('endpoint.create', compact('projects'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id         = Uuid::uuid4()->getHex();
        $name       = $request->input('name');
        $method     = $request->input('method');
        $response   = $request->input('response');
        $id_project = $request->input('id_project');

        $data = new ModelEndPoint();
        $data->id         = $id;
        $data->name       = $name;
        $data->method     = $method;
        $data->response   = $response;
        $data->id_project = $id_project;
        $data->save();
        
        return redirect('endpoint')->with('success','Endpoint has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $endpoints  = ModelEndPoint::find($id);
        $projects   = ModelProject::all();

        return view('endpoint.edit',compact('endpoints','id','projects'));        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $name = $request->input('name');
        $method = $request->input('method');
        $response = $request->input('response');
        $id_project = $request->input('id_project');

        $data = ModelEndPoint::where('id',$id)->first();
        $data->id = $id;
        $data->name = $name;
        $data->method = $method;
        $data->response = $response;
        $data->id_project = $id_project;
        $data->save();

        return redirect('endpoint');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $endpoints = ModelEndPoint::where('id', $id)->firstOrFail();
        $endpoints->delete();
        return redirect('endpoint')->with('success','Endpoint has been  deleted');
    }
}