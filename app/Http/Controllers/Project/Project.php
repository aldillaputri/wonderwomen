<?php

namespace App\Http\Controllers\Project;

use Illuminate\Http\Request;
use App\ModelProject;
use DB;
use Ramsey\Uuid\Uuid;
use App\Http\Controllers\Controller;

class Project extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects=ModelProject::all();
       
        return view('project.index',compact('projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('project.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id          = Uuid::uuid4()->getHex();
        $name        = $request->input('name');
        $description = $request->input('description');
        $slug        = $request->input('slug');

        $projects = new ModelProject();
        $projects->id = $id;
        $projects->name = $name;
        $projects->description = $description;
        $projects->slug = $slug;
        $projects->save();

        $project=ModelProject::find($id);
        return view('endpoint.create', compact('project'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $projects = ModelProject::find($id);
        return view('project.edit',compact('projects','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $name = $request->input('name');
        $description = $request->input('description');
        $slug = $request->input('slug');

        $projects = ModelProject::where('id',$id)->first();
        $projects->name = $name;
        $projects->description = $description;
        $projects->slug = $slug;
        $projects->save();

        return redirect('project');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $projects = ModelProject::where('id', $id)->firstOrFail();
        $projects->delete();
        return redirect('project')->with('success','Project has been  deleted');
    }
}