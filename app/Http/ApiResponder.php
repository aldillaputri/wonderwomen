<?php

namespace App\Http;

trait ApiResponder
{
    protected function failure($errors = ['Gagal memroses permintaan.']) {
        return response()->json([
            'errors' => is_null($errors) ? $errors : is_array($errors) ? $errors : [$errors],
        ], 400);
    }

    protected function unauthorized() {
        return response()->json([
            'errors' => [
                'Hak akses tidak tersedia.',
            ],
        ], 401);
    }

    protected function forbidden() {
        return response()->json([
            'errors' => [
                'Akses Anda dibatasi untuk layanan ini.',
            ],
        ], 403);
    }

    protected function notFound($errors = ['Item tidak ditemukan.']) {
        return response()->json([
            'errors' => is_null($errors) ? $errors : is_array($errors) ? $errors : [$errors],
        ], 404);
    }    

    protected function invalidParameters($errors = [])
    {
        return response()->json([
            'errors' => is_null($errors) ? $errors : is_array($errors) ? $errors : [$errors],
        ], 422);
    }
}