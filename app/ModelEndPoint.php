<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ModelProject;

class ModelEndPoint extends Model
{
	protected $table = 'model_end_points';
	public $incrementing = false;

    protected $fillable = [
        'name', 'method', 'response', 'slug'
    ];

    public function project()
    {
    	return $this->belongsTo(ModelProject::class, 'id_project');
    }
}