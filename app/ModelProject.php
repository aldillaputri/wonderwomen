<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ModelEndPoint;

class ModelProject extends Model
{
	protected $table = 'model_projects';
	public $incrementing = false;

    protected $fillable = [
        'name', 'description', 'slug',
    ];

    public function endpoint()
    {
        return $this->hasMany(ModelEndPoint::class);
    }
}