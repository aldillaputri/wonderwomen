@include('layouts.utils.errorMessages')

<div class="form-group row">
    {{ Form::label('name', 'Name', ['class' => 'col-sm-4 col-form-label form-label--required']) }}
    <div class="col-sm-8">
        {{ Form::text('name', null, ['class' => 'form-control']) }}
    </div>
</div>
<div class="form-group row">
    {{ Form::label('description', 'Description', ['class' => 'col-sm-4 col-form-label']) }}
    <div class="col-sm-8">
        {{ Form::text('description', null, ['class' => 'form-control']) }}
    </div>
</div>
<div class="form-group row">
    {{ Form::label('slug', 'Slug', ['class' => 'col-sm-4 col-form-label']) }}
    <div class="col-sm-8">
        {{ Form::text('slug', null, ['class' => 'form-control']) }}
    </div>
</div>