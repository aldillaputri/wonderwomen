@extends('layouts.dashboard')

@section('title', 'Project List')

@section('content')
    <div class="container-fluid">
        <h4 class="c-grey-900 mT-10 mB-30">
            <i class="fa fa-users icon-left"></i>Project List
            <a href="{{ route('project.create') }}" class="btn btn-outline-info pull-right">
                <i class="fa fa-plus icon-left"></i>New Project
            </a>
        </h4>
        <div class="row">
            <div class="col-md-12">
                <div class="bgc-white bd bdrs-3 p-20 mB-20">
                    <table id="dataTable" class="table table-bordered table-hover" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Slug</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Slug</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            @foreach  ($projects as $project)
                                <tr>
                                    <td>{{ $project->name }}</td>        
                                    <td>{{ $project->description }}</td>   
                                    <td>{{ $project->slug }}</td>
                                    <td>
                                        <div class="gap-5 peers">
                                            <div class="peer">
                                                <a href="{{ route('project.edit', $project->id) }}" class="btn btn-sm btn-outline-info">Edit</a>
                                            </div>
                                            @if (!$project->built_in)
                                                <div class="peer">
                                                    {!! Form::open(['method' => 'DELETE', 'route' => ['project.destroy', $project->id] ]) !!}
                                                        <button class="btn btn-sm btn-outline-danger" onclick="confirmDeletion(event)">Hapus</button>
                                                    {!! Form::close() !!}
                                                </div>
                                            @endif
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@include('layouts.utils.confirmDeletionScript')