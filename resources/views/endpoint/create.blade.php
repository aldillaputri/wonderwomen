@extends('layouts.dashboard')

@section('title', 'Endpoints')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <h4 class="c-grey-700 mT-10 mB-30">
                    <i class="fa fa-user-plus icon-left"></i>Endpoints
                    <a href="{{ route('project.index') }}" class="btn btn-outline-primary pull-right">
                        <i class="fa fa-list icon-left"></i>Endpoint List
                    </a>
                </h4>
                <!-- project -->
                <div class="bgc-white p-20 bd">
                    <div class="mT-30">
                        {{ Form::model($projects, ['route' => ['project.update', $projects->id], 'method' => 'PUT']) }}
                            @include('project.form')
                            <div class="row gap-50">
                            </div>
                        {{ Form::close() }}
                    </div>
                </div>
                <!-- endpoint -->
                <div class="bgc-white p-20 bd">
                    <div class="mT-30">
                        {{ Form::open(['route' => ['endpoint.index']]) }}
                            @include('endpoint.form')
                            <div class="row gap-50">
                                <div class="col-md-12">
                                    <div class="gap-10 peers">
                                        <div class="peer">
                                            <button class="btn btn-primary" type="submit">
                                                <i class="fa fa-plus icon-left"></i>
                                                Save
                                            </button>
                                        </div>
                                        <div class="peer">
                                            <a class="btn btn-light" href="{{ route('endpoint.index') }}">
                                                <i class="fa fa-arrow-left icon-left"></i>
                                                Cancel
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection