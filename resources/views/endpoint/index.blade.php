@extends('layouts.dashboard')

@section('title', 'Endpoints')

@section('content')
    <div class="container-fluid">
        <h4 class="c-grey-900 mT-10 mB-30">
            <i class="fa fa-users icon-left"></i>Endpoint List
            <a href="{{ route('endpoint.create') }}" class="btn btn-outline-info pull-right">
                <i class="fa fa-plus icon-left"></i>New Endpoint
            </a>
        </h4>
        <div class="row">
            <div class="col-md-12">
                <div class="bgc-white bd bdrs-3 p-20 mB-20">
                    <table id="dataTable" class="table table-bordered table-hover" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Method</th>
                                <th>Response</th>
                                <th>Project</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Name</th>
                                <th>Method</th>
                                <th>Response</th>
                                <th>Project</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            @foreach  ($endpoints as $endpoint)
                                <tr>
                                    <td>{{ $endpoint->name }}</td>        
                                    <td>{{ $endpoint->method }}</td>   
                                    <td>{{ $endpoint->response }}</td>
                                    <td>{{ $endpoint->project['name'] }}</td>
                                    <td>
                                        <div class="gap-5 peers">
                                            <div class="peer">
                                                <a href="{{ route('endpoint.edit', $endpoint->id) }}" class="btn btn-sm btn-outline-info">Edit</a>
                                            </div>
                                            @if (!$endpoint->built_in)
                                                <div class="peer">
                                                    {!! Form::open(['method' => 'DELETE', 'route' => ['endpoint.destroy', $endpoint->id] ]) !!}
                                                        <button class="btn btn-sm btn-outline-danger" onclick="confirmDeletion(event)">Hapus</button>
                                                    {!! Form::close() !!}
                                                </div>
                                            @endif
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@include('layouts.utils.confirmDeletionScript')