<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Selamat Datang | Artcak Technology</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300" rel="stylesheet">
    <style>
        body {
            background: #fff;
            color: #444;
            font-family: 'Open Sans', sans-serif, Arial;
            font-size: 16px;
            line-height: 28px;
            margin: 0;
            padding: 0;
        }
        p {
            margin-bottom: 64px;
        }
        footer {
            margin-top: 100px;
            padding: 50px;
            text-align: center;
        }
        .full-view, .normal-view {
            padding: 20px;
            position: relative;
        }
        .full-view {
            min-height: 500px;
        }
        .normal-view {
            min-height: 300px;
        }
        .full-view > .full-view__inner, .normal-view > .normal-view__inner {
            left: 50%;
            position: absolute;
            top: 50%;
            transform: translate(-50%, -50%);
            width: 970px;
        }
        .navigation {
            background: #238bec;
            height: 70px;
            position: fixed;
            width: 100%;
            transition: all 0.15s ease-in;
            z-index: 9;
        }
        .navigation > .navigation__inner {
            margin: auto;
            width: 970px;
        }
        .navigation__item, .navigation__brand {
            box-sizing: border-box;
            color: #fff;
            height: 70px;
            line-height: 70px;
        }
        .navigation__item {
            padding: 8px 12px;
        }
        .navigation__brand {
            font-weight: bold;
        }
        a.navigation__item, a.navigation__item:active, a.navigation__item:visited, a.navigation__item:focus {
            text-decoration: none;
        }
        .navigation--white {
            background: rgba(255, 255, 255, 0.925);
            box-shadow: 0 -5px 15px 10px #eee;
        }
        .navigation--white .navigation__item, .navigation--white .navigation__brand {
            color: #333;
        }
        .stick-left { float:left; }
        .stick-right { float:right; }
        .heading--large {
            font-size: 38px;
            font-weight: normal;
            line-height: 46px;
        }
        .button {
            background: #fefefe;
            border-radius: 100px;
            box-shadow: 0 3px 10px 3px;
            box-sizing: border-box;
            color: #1874ca;
            cursor: pointer;
            font-size: 14px;
            font-weight: bold;
            padding: 14px 32px;
            text-align: center;
        }
        a.button {
            text-decoration: none;
        }
        .button:hover {
            box-shadow: 0 5px 10px 6px;
        }
        #banner {
            background: #238bec;
            color: #fff;
            margin-bottom: 50px;
        }
        .columned__description {
            width: 500px;
        }
    </style>
</head>
<body>
    <nav>
        <div id="navigation" class="navigation">
            <div class="navigation__inner">
                <div class="stick-left">
                    <div class="navigation__brand">
                        Artcak Technology
                    </div>
                </div>
                <div class="stick-right">
                    @if (Auth::check())
                        <a class="navigation__item" href="{{ url('/dahboard') }}">Dasbor</a>
                    @else
                        <a class="navigation__item" href="{{ url('/login') }}">Masuk</a>
                        <a class="navigation__item" href="{{ url('/register') }}">Daftar</a>
                    @endif
                </div>
            </div>
        </div>
    </nav>
    <main>
        <section id="banner" class="full-view">
            <div class="full-view__inner">
                <div class="columned__description">
                    <h1 class="heading--large">Artcak Laravel Starter</h1>
                    <p>
                        Project starter yang kaya akan fitur dasar untuk mempercepat proses pengembangan perangkat lunak berbasis web.
                    </p>

                    <a class="button smooth-scroll" data-target="#manajemen-pengguna">Lihat Fitur Selengkapnya</a>
                </div>
                <img 
                    width="400" 
                    style="position: absolute; bottom: 0; right: 0; transform: translateY(25%); box-shadow: 0 3px 10px 3px rgba(10, 10, 10, 0.2);"
                    src="{{ asset('assets/static/images/banner-illustration.png') }}">
            </div>
        </section>
        <section id="manajemen-pengguna" class="full-view">
            <div class="full-view__inner">
                <div class="columned__description">
                    <h1 class="heading--large">Manajemen Pengguna dan Autentikasi</h1>
                    Model, controller dan view untuk menajamen pengguna yang meliputi:
                    <ul>
                        <li>Autentikasi via web</li>
                        <li>Manajemen pengguna</li>
                        <li>Manajemen peran</li>
                        <li>Manajemen hak akses</li>
                        <li>Dukungan pengelolaan peran dan hak akses menggunakan <strong>Laratrust</strong>
                    </ul>
                </div>
            </div>
        </section>
        <section id="integrasi-template" class="normal-view">
            <div class="normal-view__inner">
                <div class="columned__description stick-right">
                    <h1 class="heading--large">Integrasi Halaman Dasbor</h1>
                    Halaman beserta fitur-fitur dasar dasbor yang elegan dengan dukungan dari <strong>Adminator</strong> dashboard template. Serta dukungan <strong>Laravel HTML Collective</strong> yang sudah terpasang secara default.
                </div>
            </div>
        </section>
        <section id="push-notification" class="normal-view">
            <div class="normal-view__inner">
                <div class="columned__description">
                    <h1 class="heading--large">Push Notification</h1>
                    Kirim push notification kepada pengguna perangkat ponsel pintar melalui sistem back-end dengan dukungan <strong>Firebase</strong>.
                </div>
            </div>
        </section>
        <section id="sms-notification" class="normal-view">
            <div class="normal-view__inner">
                <div class="columned__description stick-right">
                    <h1 class="heading--large">Notifikasi SMS</h1>
                    Jangkau pengguna melalui layanan SMS untuk pengiriman notifikasi maupun One Time Password (OTP) menggunakan kanal SMS Gateway pilihan Anda. 
                </div>
            </div>
        </section>
        <section id="api-dasar" class="normal-view">
            <div class="normal-view__inner">
                <div class="columned__description">
                    <h1 class="heading--large">API Dasar</h1>
                    Ciptakan komunikasi antara sistem back-end dengan mobile app melalui REST API. Tersedia dua pilihan format respon API yang bisa disesuaikan dengan kebutuhan sistem. 
                    <br></br>
                    Pastikan API sudah memenuhi kebutuhan fungsionalitas dengan menjalankan API testing dengan contoh yang sudah tersedia. Serta dukungan <strong>Swagger UI</strong> untuk membangun dokumentasi API dengan format open API specification.
                </div>
            </div>
        </section>
    </main>
    <footer>
        &copy; {{ date('Y') }}. Crafted with &hearts; by Artcak Technology.
    </footer>
    <script>
        function fadeInNavigation() {
            const nav = document.querySelector('#navigation')
            if (nav.className.indexOf('navigation--white') ===  -1) {
                nav.className = 'navigation navigation--white'
            }
        }

        function fadeOutNavigation() {
            const nav = document.querySelector('#navigation')
            if (nav.className.indexOf('navigation--white') >  -1) {
                nav.className = 'navigation'
            }
        }

        window.addEventListener('scroll', function(event) {
            if (event.pageY > 500) fadeInNavigation()
            else fadeOutNavigation()
        })

        document.querySelectorAll('.smooth-scroll').forEach(function(item) {
            item.addEventListener('click', function(event) {
                const button = event.target
                const target = button.dataset.target
                const targetElement = document.querySelector(target)
                const position = targetElement.offsetTop
                
                
                const scroller = setInterval(function() {
                    let currentPosition = window.pageYOffset
                    window.scrollTo(0, currentPosition + 10)
                    if (window.pageYOffset >= targetElement.offsetTop) {
                        clearInterval(scroller)
                    }
                }, 1)
            })
        })
    </script>
</body>
</html>