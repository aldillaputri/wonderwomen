<?php

namespace Tests\Feature\Api\V1\Autentikasi;

use Tests\Feature\Api\V1\EncapsulatedApiTestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use DB;

class LoginTest extends EncapsulatedApiTestCase
{
    use DatabaseTransactions;
    protected $user;
    protected $password;

    protected function setUp()
    {
        parent::setUp();        
        $this->password = 'secret';
        $this->user = $this->createUser([
            'password' => $this->password,
        ]);
    }

    protected function resetUser()
    {
        $this->user->token_login_api = NULL;
        $this->user->save();
    }

    public function testLogin()
    {
        $this->resetUser();
        $response = $this->json('POST', $this->urls['login'], [
            'username' => $this->user->username,
            'password' => $this->password,
        ]);
        $token = $this->getDataItem($response, 'token');
        $updatedUser = DB::table('users')->where('id', $this->user->id)->first();
        $this->returnSuccess($response, 'Login sukses.');
        $this->dataHasKey($response, 'token');
        $this->assertTrue($token == $updatedUser->token_login_api);
    }

    public function testLoginUsingInvalidPassword()
    {
        $this->resetUser();
        $response = $this->json('POST', $this->urls['login'], [
            'username' => $this->user->username,
            'password' => 'invalid'.$this->password,
        ]);
        $updatedUser = DB::table('users')->where('id', $this->user->id)->first();
        $this->returnFailure($response, 'Password tidak sesuai.');        
        $this->assertTrue($updatedUser->token_login_api === NULL);
    }

    public function testLoginForUnavailableUser()
    {
        $this->resetUser();
        $response = $this->json('POST', $this->urls['login'], [
            'username' => 'invalid'.$this->user->username,
            'password' => 'invalid'.$this->password,
        ]);
        $updatedUser = DB::table('users')->where('id', $this->user->id)->first();
        $this->returnNotFound($response, 'User tidak ditemukan.');        
        $this->assertTrue($updatedUser->token_login_api === NULL);
    }

    public function testLoginWithoutUsername()
    {
        $this->resetUser();
        $response = $this->json('POST', $this->urls['login'], [
            'password' => $this->password,
        ]);
        $updatedUser = DB::table('users')->where('id', $this->user->id)->first();
        $this->returnInvalidParameters($response, [
            'Bidang isian username wajib diisi.',
        ]);      
        $this->assertTrue($updatedUser->token_login_api === NULL);
    }

    public function testLoginWithoutPassword()
    {
        $this->resetUser();
        $response = $this->json('POST', $this->urls['login'], [
            'username' => $this->user->username,
        ]);
        $updatedUser = DB::table('users')->where('id', $this->user->id)->first();
        $this->returnInvalidParameters($response, [
            'Bidang isian password wajib diisi.',
        ]);      
        $this->assertTrue($updatedUser->token_login_api === NULL);
    }
}