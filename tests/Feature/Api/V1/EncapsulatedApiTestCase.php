<?php

namespace Tests\Feature\Api\V1;

use Tests\TestCase;
use PHPUnit\Framework\Assert as PHPUnit;
use DB;
use App\User;

class EncapsulatedApiTestCase extends TestCase
{
    protected $responseStructure = [
        'response_code',
        'message',
        'errors',
        'data',
    ];
    protected $urls = [
        'login' => '/api/v1/autentikasi/login',
    ];
    protected $loginToken = '';
    
    protected function addUrls($urls = [])
    {
        $this->urls = array_merge($this->urls, $urls);
        return $this->urls;
    }

    protected function loginUser($userProperties = [])
    {
        $user = $this->createUser($userProperties);
        $response = $this->json('POST', $this->urls['login'], [
            'username' => $user->username,
            'password' => 'secret',
        ]);
        $token = $this->getDataItem($response, 'token');
        $user = User::find($user->id);
        return [
            'user' => $user,
            'token' => $token,
        ];
    }

    protected function createUser($properties = [])
    {
        $user = factory(\App\User::class)->create(array_merge([
            'password' => 'secret',
        ], $properties));
        return $user;
    }
    
    protected function returnSuccess($response, $expectedMessage = 'Permintaan berhasil diproses.') 
    {
        return $response
            ->assertStatus(200)
            ->assertJsonStructure($this->responseStructure)
            ->assertJson([
                'response_code' => 200,
                'message' => $expectedMessage,
                'errors' => null,
            ]);
    }

    protected function returnFailure($response, $errors = ['Gagal memroses permintaan.'])
    {
        return $response
            ->assertStatus(200)
            ->assertJsonStructure($this->responseStructure)
            ->assertJson([
                'response_code' => 400,
                'message' => NULL,
                'data' => NULL,
                'errors' => is_null($errors) ? $errors : is_array($errors) ? $errors : [$errors],
            ]);
    }

    protected function returnUnauthorized($response)
    {
        return $response
            ->assertStatus(200)
            ->assertJsonStructure($this->responseStructure)
            ->assertJson([
                'response_code' => 401,
                'message' => NULL,
                'data' => NULL,
            ]);
    }

    protected function returnForbidden($response)
    {
        return $response
            ->assertStatus(200)
            ->assertJsonStructure($this->responseStructure)
            ->assertJson([
                'response_code' => 403,
                'message' => NULL,
                'data' => NULL,
            ]);
    }

    protected function returnNotFound($response, $errors = ['Item tidak ditemukan.'])
    {
        return $response
            ->assertStatus(200)
            ->assertJsonStructure($this->responseStructure)
            ->assertJson([
                'response_code' => 404,
                'message' => NULL,
                'data' => NULL,
                'errors' => is_null($errors) ? $errors : is_array($errors) ? $errors : [$errors],
            ]);
    }

    protected function returnInvalidParameters($response, $errors = [])
    {
        return $response
            ->assertStatus(200)
            ->assertJsonStructure($this->responseStructure)
            ->assertJson([
                'response_code' => 422,
                'message' => NULL,
                'data' => NULL,
                'errors' => is_null($errors) ? $errors : is_array($errors) ? $errors : [$errors],
            ]);
    }

    protected function hasExactData($response, $expectedData)
    {
        return $response->assertJson([
            'data' => $expectedData,
        ]);
    }

    protected function dataHasStructure($response, $expectedStructure = [])
    {
        $originalResponse = $response->original;
        foreach ($expectedStructure as $key) {
            PHPUnit::assertArrayHasKey($key, $originalResponse['data']);
        }
    }

    protected function dataHasKey($response, $key)
    {
        $originalData = (array) $response->original['data'];
        $this->assertTrue(array_key_exists($key, $originalData));
    }

    protected function dataHasItem($response, $key, $value)
    {
        $originalData = (array) $response->original['data'];
        $this->assertTrue(array_key_exists($key, $originalData));
        $this->assertTrue($originalData[$key] == $value);
    }

    protected function getDataItem($response, $key)
    {
        $originalData = (array) $response->original['data'];
        return $originalData[$key];
    }

    protected function getData($response)
    {
        return ($response->original)['data'];
    }

    protected function hasError($response, $error = '')
    {
        $originalResponse = $response->original;
        PHPUnit::assertContains($error, $originalResponse['errors']);
    }
}